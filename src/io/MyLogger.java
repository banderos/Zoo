package io;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MyLogger {
    public static void log(String str) {
        Date date=new Date();
        Calendar c=new GregorianCalendar();
        DateFormat df=new SimpleDateFormat("dd_MM_YYYY");
        DateFormat timeFormat= new SimpleDateFormat("YYYY_MM_dd HH:mm:ss.SSS");
        String fileName="../log_"+df.format(date)+".txt";
        File file = new File(fileName);
        try {
            PrintWriter bw = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
            bw.println(timeFormat.format(date)+ " " + str);
            bw.flush();
            bw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
