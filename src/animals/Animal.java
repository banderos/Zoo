package animals;

import java.util.Random;
import java.util.UUID;

import error.AnimalCreationException;
import interfaces.Jumpable;
import interfaces.Soundable;

public abstract class Animal implements Soundable, Jumpable, Comparable<Animal> {
	protected String type;
	protected double fill;
	protected long createdAt;
	protected long lastFeedTime;
	boolean isAlive = true;
	private double size;
	private String nickname;
	private IDeadAnimalListener deadAnimalListener;

	public Animal(double size, String nickname) {
		this.size = size;
		this.nickname = nickname;
		setFill(120);
		this.createdAt = System.currentTimeMillis();
	}

	protected void die() {
		this.isAlive = false;
		if (deadAnimalListener != null) {
			deadAnimalListener.onAnimalDeath(this);
		}
	}

	@Override
	public String toString() {
		return (getType() + "\t" + getNickname() + "\t" + getSize() + "\t");
	}

	public int compareTo(Animal o) {
		return (int) Math.ceil(this.getSize() - o.getSize());
	}

	private final String getType() {
		return type;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public double getFill() {
		if (((System.currentTimeMillis() - lastFeedTime) / 1000) > fill) {
			die();
		}
		return fill;
	}

	public void setFill(double fill) {
		this.fill = fill;
		lastFeedTime = System.currentTimeMillis();
	}

	public void setDeadAnimalListener(IDeadAnimalListener deadAnimalListener) {
		this.deadAnimalListener = deadAnimalListener;
	}

	public double feed(double value) {
		setFill(getFill() + value);
		return getFill();
	}

	public static Animal convertFromString(String input)
			throws AnimalCreationException {
		String[] values = input.split(",");
		switch (values[0]) {
		case "Cat":
			return new DomesticCat(Double.parseDouble(values[2]), values[1]);
		case "Wolf":
			return new ForestWolf(Double.parseDouble(values[2]), values[1]);
		case "Raven":
			return new Raven(Double.parseDouble(values[2]), values[1]);
		case "Rabbit":
			return new Rabbit(Double.parseDouble(values[2]), values[1]);
		}
		throw new AnimalCreationException();
	}

	public interface IDeadAnimalListener {
		void onAnimalDeath(Animal animal);
	}

}
