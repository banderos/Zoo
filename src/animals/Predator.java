package animals;

public abstract class Predator extends Land {
    private boolean isScavenger;

    public Predator(double size, String nickname) {
        super(size, nickname);
    }

    public boolean isScavenger() {
        return isScavenger;
    }

    public void setScavenger(boolean scavenger) {
        isScavenger = scavenger;
    }

    public void consume(Herbivore prey) {
        this.feed(prey.getSize()*10);
        prey.onConsumption();
        System.out.println(prey.type + " " + prey.getNickname() + " has been consumed by " + this.type + " " + this.getNickname());
        return;
    }
}
