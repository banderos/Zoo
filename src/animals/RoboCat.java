package animals;

public class RoboCat extends Felinae {
    private static final double DEFAULT_CAT_SIZE = 10;
    private static int catCounter = 0;


    private VoiceModule voiceModule = new VoiceModule();

    public RoboCat() {
        super(DEFAULT_CAT_SIZE, "#" + catCounter++);
        type = "Cybocat";
    }

    @Override
    public double jump() {
        return 1000;
    }

    @Override
    public void sound() {
        System.out.println(voiceModule.generateSound());
    }

    public VoiceModule getVoiceModule() {
        return voiceModule;
    }

    public void setVoiceModule(VoiceModule voiceModule) {
        this.voiceModule = voiceModule;
    }

    public static class VoiceModule {
        private String sound = "00000000";
        public String getSound() {
            return sound;
        }

        public void setSound(String sound) {
            this.sound = sound;
        }

        public String generateSound() {
            return sound;
        }
    }
}

