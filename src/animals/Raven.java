package animals;

import input.Input;

public class Raven extends Bird {
    public Raven(double size, String nickname) {
        super(size, nickname);
        type = "Raven";
    }

    public static Raven createRaven() {
        double size = Input.megaInputNumber("Enter your raven's size");
        String name = Input.megaInputString("Enter your raven's name");
        return new Raven(size, name);
    }

    @Override
    public double jump() {
        return getSize() / 0.56;
    }

    @Override
    public void sound() {
        System.out.println("Kra");
    }
}

