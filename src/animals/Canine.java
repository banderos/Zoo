package animals;

public abstract class Canine extends Predator {
    public Canine(double size, String nickname) {
        super(size, nickname);
    }
}
