package input;

import animals.Animal;
import jdk.management.resource.internal.inst.FileInputStreamRMHooks;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

import error.AnimalCreationException;
import static main.Main.scan;

public class Input {
	public static double megaInputNumber(String str) {
		System.out.println(str);
		while (true) {
			String input = scan.nextLine();
			try {
				return Double.parseDouble(input.replace(",", "."));
			} catch (NumberFormatException e) {
				System.out.println("Please enter number");
			}
		}
	}

	public static String inputFromConsole() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		int temp;
		try {
			while ((temp = br.read()) != -1 && temp != 10) {
				sb.append((char) temp);
			}
		} catch (IOException e) {
			System.out.println("This not be a happen");
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
	public static String filePathUser()
	{
		System.out.println("Enter path of neo");
		String path = scan.nextLine();
		return path;
	}

	
	   public static List<Animal> importFromFile() throws AnimalCreationException {
	        File file = new File(filePathUser()+".csv");
	        List<Animal> listAnimal = new LinkedList<>();
	        if (!file.exists()) {
	            System.out.println("File not exists.");
	        }
	        try {
	            StringBuilder sb = new StringBuilder();
	            BufferedReader br = new BufferedReader(new FileReader(file));
	            int nextChar, tmp = 0;
	            while ((nextChar = br.read()) != -1) {
	                if (nextChar >= 32) {
	                    sb.append((char) nextChar);
	                } else if (nextChar == 10 && tmp == 13) {
	                    listAnimal.add(Animal.convertFromString(sb.toString()));
	                    sb.setLength(0);
	                }
	                tmp = nextChar;
	            }
	            if (sb.length() > 0) {
	                listAnimal.add(Animal.convertFromString(sb.toString()));
	                sb.setLength(0);
	            }
	        } catch (IOException e) {
	            System.out.println(e.getMessage());
	        }
	        return listAnimal;
	    }


	public static String megaInputString(String str) {
		System.out.println(str);
		return scan.nextLine();
	}
}
