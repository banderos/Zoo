package main;

public class Input {
    public static double megaInputNumber(String str) {
        System.out.println(str);
        while (true) {
            String input = Main.scan.nextLine();
            try {
              return  Double.parseDouble(input.replace(",", "."));

            } catch (NumberFormatException e) {
                System.out.println("incorrect input, please try again "+e.getMessage());
            }
        }

    }
}
